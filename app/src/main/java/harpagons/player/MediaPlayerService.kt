package harpagons.player

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import java.io.IOException

class MediaPlayerService : Service() {

	private val mpBinder = MediaPlayerBinder()

	private var mp = MediaPlayer()

	private var onCompletionListener: ((MediaPlayer)->Unit)? = null

	enum class State { IDLE, READY, END }

	var state = State.IDLE
		private set

	val currentPosition: Int
		get() = if (state == State.READY) mp.currentPosition else 0

	val duration: Int
		get() = if (state == State.READY) mp.duration else 0

	val isPlaying: Boolean
		get() = state == State.READY && mp.isPlaying

	inner class MediaPlayerBinder : Binder() {
		val service: MediaPlayerService
			get() = this@MediaPlayerService
	}

	override fun onBind(intent: Intent): IBinder? = mpBinder

	fun setSong(path: String): State {
		reset()
		return try {
			mp.setDataSource(path)
			mp.prepare()
			state = State.READY
			state
		} catch (ie: IOException) {
			state
		}
	}

	fun release() {
		mp.release()
		state = State.END
	}

	fun reset() {
		if (state == State.END) {
			mp = MediaPlayer()
			mp.setOnCompletionListener(onCompletionListener)
		} else {
			mp.reset()
		}
		state = State.IDLE
	}

	fun seekTo(time: Int) {
		if (state == State.READY)
			mp.seekTo(time)
	}

	fun setOnCompletionListener(l: (MediaPlayer) -> Unit) {
		onCompletionListener = l
		mp.setOnCompletionListener(l)
	}

	fun toggle() {
		if (state == State.READY) {
			if (mp.isPlaying) {
				mp.pause()
			} else {
				mp.start()
			}
		}
	}
}
