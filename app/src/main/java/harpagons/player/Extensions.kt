package harpagons.player

import android.view.View
import android.widget.AbsListView
import android.widget.ArrayAdapter

fun <T> ArrayAdapter<T>.buildArrayList(): ArrayList<T> {
	val result = ArrayList<T>()
	for (i in 0 until this.count)
		result.add(this.getItem(i)!!)
	return result
}

fun AbsListView.getViewByPosition(pos: Int): View {
	val first = this.firstVisiblePosition
	val last = first + this.childCount - 1
	return if (pos < first || pos > last) {
		this.adapter.getView(pos, null, this)
	} else {
		this.getChildAt(pos - first)
	}
}
