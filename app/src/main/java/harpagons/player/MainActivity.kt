package harpagons.player

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AbsListView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_empty.*
import java.io.File
import java.io.IOException
import kotlin.collections.set


class MainActivity : AppCompatActivity() {

	private val rootPath = Environment.getExternalStorageDirectory().path
	private val confPath = "$rootPath/Documents/.player_paths.conf"
	private val bgPath = "$rootPath/Pictures/.player_bg.jpg"

	private val musicPaths = HashMap<String, ArrayList<String>>()

	private var musicFoldersAdapter: ArrayAdapter<String>? = null
	private lateinit var musicFilesAdapter: ArrayAdapter<String>
	private lateinit var playlistAdapter: ArrayAdapter<String>

	private lateinit var currentFolderPath: String
	private var playlistPosition: Int = -1

	private var mps = MediaPlayerService()

	private val handler = Handler()
	private var runnable: Runnable? = null

	private val m3uPattern = ".*\\.m3u\$".toRegex()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		checkPermission()
		readFiles()
		if (musicPaths.count() < 1) {
			setContentView(R.layout.activity_main_empty)
			configFileInfoView.text = resources.getString(R.string.confFileInfo).format(confPath)
		} else {
			setContentView(R.layout.activity_main)
			initPlaylistNameView()
			initSeekBar()
			initClearButton()
			initShuffleButton()
			initSaveButton()
			initStopButton()
			initFolderView()
			initFilesView()
			initPlaylistView()
			initMPS()
			BG.set(parentLayout, bgPath, resources)
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		if (runnable != null)
			handler.removeCallbacks(runnable!!)
	}

	private fun checkPermission() {
		while (ContextCompat.checkSelfPermission(
				this,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
			) != PackageManager.PERMISSION_GRANTED
		) {
			val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
			ActivityCompat.requestPermissions(this, permissions, 0)
		}
	}

	private fun readFiles() {
		val confFile = File(confPath)
		if (!confFile.exists()) return
		for (line in confFile.readLines()) {
			val path = "$rootPath/$line"
			try {
				val content = File(path).listFiles()
				if (content != null) {
					val files = ArrayList<String>()
					content.forEach {
						if (it.isFile) files.add(it.name)
					}
					musicPaths[path] = files
				}
			} catch (e: IOException) {}
		}
	}

	private fun initPlaylistNameView() {
		playlistNameView.setOnFocusChangeListener { v, hasFocus ->
			if (!hasFocus) {
				val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
				imm.hideSoftInputFromWindow(v.windowToken, 0)
			}
		}
	}

	private fun initSeekBar() {
		seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

			override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
				if (fromUser)
					mps.seekTo(progress)
			}

			override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

			override fun onStopTrackingTouch(seekBar: SeekBar?) {
				if (seekBar != null)
					mps.seekTo(seekBar.progress)
			}
		})
	}

	private fun initSaveButton() = savePlaylistButton.setOnClickListener {
		val prefix = playlistNameView.text.toString()
		val name = Name(currentFolderPath, "m3u").findAvailable(prefix)
		File(name).printWriter().use { out ->
			for (i in 0 until playlistAdapter.count) {
				out.println(playlistAdapter.getItem(i))
			}
		}
	}

	private fun initShuffleButton() = shuffleButton.setOnClickListener {
		val count = playlistAdapter.count
		val a = ArrayList<String>()
		for (i in 0 until count) {
			a.add(playlistAdapter.getItem(i)!!)
		}
		a.shuffle()
		playlistAdapter = ArrayAdapter(this, R.layout.entry, a)
		playlistView.adapter = playlistAdapter
	}

	private fun initClearButton() = clearButton.setOnClickListener {
		playlistAdapter.clear()
	}

	private fun initStopButton() = stopButton.setOnClickListener {
		setPlaylistPosition(-1)
		nudgeMPS()
	}

	// * musicPaths must have at least 1 item to call this safely.
	private fun initFolderView() {
		val musicPathsList = musicPaths.keys.toList()
		currentFolderPath = musicPathsList[0]
		if (musicPathsList.count() == 1) {
			musicFoldersView.visibility = View.GONE
		} else {
			musicFoldersAdapter = ArrayAdapter(
				this, R.layout.entry, musicPathsList
			)
			musicFoldersView.adapter = musicFoldersAdapter
			musicFoldersView.setOnItemClickListener { _, _, pos, _ ->
				currentFolderPath = musicFoldersAdapter!!.getItem(pos)!!
				playlistAdapter.clear()
				musicFilesAdapter.clear()
				musicFilesAdapter.addAll(musicPaths[currentFolderPath]!!)
			}
			musicFoldersView.setOnItemLongClickListener { _, _, pos, _ ->
				currentFolderPath = musicFoldersAdapter!!.getItem(pos)!!
				val mp = musicPaths[currentFolderPath]!!
				playlistAdapter.clear()
				musicFilesAdapter.clear()
				musicFilesAdapter.addAll(mp)
				mp.forEach {
					if (!m3uPattern.matches(it)) {
						playlistAdapter.add(it)
					}
				}
				true
			}
		}
	}

	// * musicPaths must have at least 1 item to call this safely.
	private fun initFilesView() {
		musicFilesAdapter = ArrayAdapter(
			this,
			R.layout.entry,
			musicPaths[currentFolderPath]!!
		)
		musicFilesView.adapter = musicFilesAdapter
		musicFilesView.setOnItemClickListener { _, _, pos, _ ->
			val s = musicFilesAdapter.getItem(pos)
			if (m3uPattern.matches(s!!)) {
				File("$currentFolderPath/$s").readLines()
					.forEach(playlistAdapter::add)
			} else {
				playlistAdapter.add(s)
			}
		}
	}

	private fun initPlaylistView() {
		playlistAdapter = ArrayAdapter(
			this, R.layout.entry, ArrayList<String>()
		)
		playlistView.adapter = playlistAdapter
		playlistView.setOnItemClickListener { _, _, pos, _ ->
			if (playlistPosition == pos) {
				mps.toggle()
			} else {
				setPlaylistPosition(pos)
				nudgeMPS()
			}
		}
		playlistView.setOnItemLongClickListener { _, _, pos, _ ->
			val items = playlistAdapter.buildArrayList()
			items.removeAt(pos)
			playlistAdapter = ArrayAdapter(this, R.layout.entry, items)
			playlistView.adapter = playlistAdapter
			if (playlistPosition >= items.count())
				setPlaylistPosition(0)
			true
		}
		playlistView.setOnScrollListener(object : AbsListView.OnScrollListener {
			override fun onScroll(
				v: AbsListView,
				firstVisible: Int,
				visibleCount: Int,
				totalCount: Int
			) {
				val lastVisible = firstVisible + visibleCount
				for (i in firstVisible until lastVisible) {
					v.getViewByPosition(i).setBackgroundColor(
						ContextCompat.getColor(
							applicationContext,
							R.color.transparent
						)
					)
				}
				if (playlistPosition in firstVisible until lastVisible) {
					v.getViewByPosition(playlistPosition).setBackgroundColor(
						ContextCompat.getColor(
							applicationContext,
							R.color.colorAccent
						)
					)
				}
			}

			override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) = Unit
		})
	}

	private fun setPlaylistPosition(i: Int) {
		if (playlistPosition in 0 until playlistAdapter.count) {
			playlistView.getViewByPosition(playlistPosition).setBackgroundColor(
				ContextCompat.getColor(this, R.color.transparent)
			)
		}
		playlistPosition = if (i in 0 until playlistAdapter.count) {
			playlistView.getViewByPosition(i).setBackgroundColor(
				ContextCompat.getColor(this, R.color.colorAccent)
			)
			i
		} else {
			-1
		}
	}

	private fun initMPS() {
		mps.setOnCompletionListener {
			setPlaylistPosition(playlistPosition + 1)
			nudgeMPS()
		}
	}

	private fun seekBarUpdate() {
		seekBar.progress = mps.currentPosition
		if (runnable != null)
			handler.postDelayed(runnable!!, 1000)
	}

	private fun nudgeMPS() {
		if (playlistPosition > -1) {
			mps.setSong(getCurrentSongPath())
			mps.toggle()
			runnable = Runnable(this::seekBarUpdate)
			seekBar.max = mps.duration
			seekBarUpdate()
		} else {
			mps.release()
			setPlaylistPosition(-1)
			runnable = null
		}
	}

	private fun getCurrentSongPath(): String =
		"$currentFolderPath/${playlistAdapter.getItem(playlistPosition)}"
}
