package harpagons.player

import java.io.File

class Name(private val dst: String, private val ext: String) {

	private val dstFile = File(dst)

	private fun formName(prefix: String, i: Int): String {
		return if (prefix == "")
			"$i.$ext"
		else
			"$prefix-$i.$ext"
	}

	private fun alreadyExists(name: String): Boolean {
		dstFile.walk().forEach {
			if (name == it.name) return true
		}
		return false
	}

	fun findAvailable(prefix: String): String {
		var i = 1
		var name = formName(prefix, i)
		while (alreadyExists(name)) {
			i += 1
			name = formName(prefix, i)
		}
		return "$dst/$name"
	}
}
