package harpagons.player

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.view.View

private class ImageZoomParams(pW: Int, pH: Int, imgW: Int, imgH: Int) {
	val x: Int
	val y: Int
	val w: Int
	val h: Int
	init {
		val imgRatio = imgW.toFloat() / imgH.toFloat()
		val pRatio = pW.toFloat() / pH.toFloat()
		if (imgRatio >= pRatio) {
			w = (imgH * pRatio).toInt()
			h = imgH
		} else {
			w = imgW
			h = (imgW / pRatio).toInt()
		}
		x = (imgW - w) / 2
		y = (imgH - h) / 2
	}
}

object BG {
	fun set(view: View, path: String, resources: Resources): Boolean {
		val orig = BitmapFactory.decodeFile(path) ?: return false
		view.addOnLayoutChangeListener { v, _, _, _, _, _, _, _, _ ->
			val w = v.width
			val h = v.height
			if (w == 0 || h == 0) {
				return@addOnLayoutChangeListener
			}
			val params = ImageZoomParams(w, h, orig.width, orig.height)
			val zoomed = Bitmap.createBitmap(orig, params.x, params.y, params.w, params.h)
			val bg = BitmapDrawable(resources, zoomed)
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				v.background = bg
			} else {
				v.setBackgroundDrawable(bg)
			}
		}
		return true
	}
}