# Player

Relatively simple application to play music from chosen folders.

## Usage

Three lists are available:

* folder list (loaded from __~/Documents/.player\_paths.conf__)
* folder content list
* playlist

__Current folder__ is chosen via folder list.

__Playlist is filled__ up by clicking on folder content list.

To __start__ playing items on playlist click an item to start with.

To __pause__, click currently played entry.

Long playlist click __removes__ an item.

__Shuffle__ button shuffles the playlist.

__Clear__ button clears playlist.

__Stop__ button releases media player and uninitializes playlist position.

__Save__ button saves playlist to __current folder__.

File name format is: __~/[current-folder]/PREFIX-NUMBER.m3u__

Or in case no prefix is given: __~/recorded/NUMBER.m3u__

__PREFIX__ is set using text field.

__NUMBER__ number is chosen depending on already existing files.

## Config

Places to read music from are configured via
__~/Documents/.player\_paths.conf__.

App attemps to read __~/Pictures/.player\_bg.jpg__ file, to set up as
background.

Small personal touch to an interface is always nice I think.

## Possible TODO

* Make proper service out of MediaPlayerService class. This
will probably require it to have its own playlist.
* Detect file changes and reaload folder content.
* Simplify? Seriously, I failed to make this project/app simple.

## LICENSE

Copyright 2020 Michał‚ Kloc

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

